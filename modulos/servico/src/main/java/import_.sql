CREATE SEQUENCE SEQ_TIPO_TELEFONE START 1;
CREATE SEQUENCE SEQ_TIPO_ENDERECO START 1;
CREATE SEQUENCE SEQ_TIPO_TABELA_FINANCEIRA START 1;
CREATE SEQUENCE SEQ_PRODUTO_FINANCEIRO START 1;
ALTER TABLE tipotelefone ADD COLUMN codigosisdia bigint;
ALTER TABLE tipoendereco ADD COLUMN codigosisdia bigint;

<!-- SEGUNDA ENTREGA -->
alter table cliente drop column cpfcnpj;
alter table cliente add column cpf varchar(255);
alter table cliente add column cnpj varchar(255);
alter table cliente add column dataNascimento date;
alter table telefone add column id_cliente int;

alter table telefone add column tipo varchar(20);
ALTER TABLE telefone ADD CONSTRAINT fk_cliente FOREIGN KEY (id_cliente) REFERENCES cliente(id);

alter table telefone add column data_hora_inclusao timestamp without time zone;
alter table telefone add column data_hora_alteracao timestamp without time zone;
alter table cliente add column sexo varchar;

alter table cliente add column rg varchar;
alter table cliente add column orgao_emissor varchar;
alter table cliente add column uf_orgao_emissor varchar;
alter table cliente add column data_emissao_rg date;
alter table cliente add column estado_civil varchar;
alter table cliente add column nome_mae varchar;
alter table cliente add column nome_pai varchar;
alter table cliente add column possui_cnh boolean;
alter table cliente add column nacionalidade varchar;


alter table cliente add column id_estado_nascimento int;
alter table cliente add column id_municipio_nascimento int;
ALTER TABLE cliente ADD CONSTRAINT fk_municipio_nascimento FOREIGN KEY (id_municipio_nascimento) REFERENCES municipio(id);
ALTER TABLE cliente ADD CONSTRAINT fk_estado_nascimento FOREIGN KEY (id_estado_nascimento) REFERENCES estado(id);
alter table cliente add column escolaridade varchar;




<!--TESTE -->
insert into tipotelefone values (1,true, 1, 'teste2');
insert into tipotelefone values (2,true, 1, 'teste2');
insert into tipotelefone values (3,true, 1, 'teste3');
insert into tipotelefone values (4,true, 1, 'teste4');
insert into tipotelefone values (5,true, 1, 'teste5');
insert into tipotelefone values (6,true, 1, 'teste6');
insert into tipotelefone values (7,true, 1, 'teste7');
insert into tipotelefone values (8,true, 1, 'teste8');
insert into tipotelefone values (9,true, 1, 'teste9');
insert into tipotelefone values (10,true, 1, 'teste10');
insert into tipotelefone values (11,true, 1, 'teste11');
insert into tipotelefone values (12,true, 1, 'teste12');
insert into tipotelefone values (13,true, 2, 'abc');
insert into tipotelefone values (14,true, 2, 'def');
insert into tipotelefone values (15,true, 2, 'ghi');
insert into tipotelefone values (16,true, 2, 'jkl');

<!--TESTE segunda entrega -->

insert into municipio_santander values (1,'Aparecida de Goiânia');
insert into nacionalidadesantander values (1,'Brasileiro');
insert into estado_santander values (1,'Goiás');
insert into estado values (1, '1','Goiás', 'GO');
insert into cidade values (1, '74905110', 'Aparecida de Goiânia', '1');
insert into profissaosantander values (1, 'Analista de Sistemas', 'ADMINISTRADOR');
insert into bancos values (1, 'SIM', 'Banco do Brasil', 1, 'bb.com.br');
insert into municipio values (1, '1', 'Aparecida de Goiânia', 'GO');