package br.com.gruposaga.cad.svs.dao.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import com.svs.fin.model.entities.AvaliacaoUsado;
import com.svs.fin.model.enums.AvaliacaoStatusEnum;

import br.com.magmatecnologia.architecture.utils.jpa.CrudService;
import br.com.magmatecnologia.architecture.utils.jpa.QueryParameter;
import br.com.magmatecnologia.architecture.utils.services.Service;

@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class AvaliacaoUsadoService extends Service<AvaliacaoUsado> {

	@Inject
	private CrudService crudService;

	@Override
	protected CrudService getCrudService() {
		return crudService;
	}

	@Override
	protected Class<AvaliacaoUsado> getClassOfT() {
		return AvaliacaoUsado.class;
	}

	public List<AvaliacaoUsado> obterPorStatusEPlaca(String placa, AvaliacaoStatusEnum avaliacaoStatusEnum) {
		List<AvaliacaoUsado> retorno = crudService.findWithNamedQuery(AvaliacaoUsado.NQ_OBTER_POR_PLACA_E_STATUS.NAME,
				QueryParameter.with(AvaliacaoUsado.NQ_OBTER_POR_PLACA_E_STATUS.NM_PM_PLACA, placa)
						.and(AvaliacaoUsado.NQ_OBTER_POR_PLACA_E_STATUS.NM_PM_STATUS, avaliacaoStatusEnum).parameters());
		return retorno;
	}

}
