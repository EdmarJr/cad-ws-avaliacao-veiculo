package br.com.gruposaga.cad.svs.ws.resources;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.entities.AvaliacaoUsado;
import com.svs.fin.model.enums.AvaliacaoStatusEnum;

import br.com.gruposaga.cad.svs.dao.services.AvaliacaoUsadoService;
import br.com.gruposaga.cad.svs.fin.ws.api.WsAvaliacaoUsados;
import br.com.magmatecnologia.architecture.utils.common.UtilsData;
import br.com.magmatecnologia.architecture.utils.common.UtilsEnum;
import br.com.magmatecnologia.architecture.utils.jpa.model.dto.listagemtabelalazy.GrupoDeParametrosParaListagemLazy;

@Path("/avaliacaoUsados")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class AvaliacaoUsadosResource implements WsAvaliacaoUsados {

	@Inject
	private AvaliacaoUsadoService avaliacaoUsadoService;

	@GET
	public Response buscarUnicoResultadoOuListaDeResultadosPaginada(
			@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("placa") String placa,
			@QueryParam("statusDaAvaliacao") String statusDaAvaliacao, @QueryParam("dataVigente") Long dataVigente) {
		Object jsonObjectRetorno = null;
		if (statusDaAvaliacao != null && placa != null && dataVigente != null) {
			List<AvaliacaoUsado> retorno = obterPorPlacaStatusEDataVigente(placa, statusDaAvaliacao, dataVigente);
			jsonObjectRetorno = retorno;
		} else if (statusDaAvaliacao != null && placa != null) {
			List<AvaliacaoUsado> listaPorPlacaEStatus = obterPorStatusEPlaca(placa, statusDaAvaliacao);
			jsonObjectRetorno = listaPorPlacaEStatus;
		} else {
			GrupoDeParametrosParaListagemLazy parametros = GrupoDeParametrosParaListagemLazy.Builder
					.novoGrupo(parametrosDeOrdenacao, parametrosDeBusca, offSet, limit).build();
			jsonObjectRetorno = avaliacaoUsadoService.obterTodos(parametros);
		}

		return Response.ok(jsonObjectRetorno).build();
	}

	private List<AvaliacaoUsado> obterPorPlacaStatusEDataVigente(String placa, String statusDaAvaliacao,
			Long dataVigente) {
		Calendar dataInformadaCalendar = Calendar.getInstance();
		dataInformadaCalendar.setTimeInMillis(dataVigente);
		List<AvaliacaoUsado> retorno = new ArrayList<AvaliacaoUsado>();
		List<AvaliacaoUsado> listaPorPlacaEStatus = obterPorStatusEPlaca(placa, statusDaAvaliacao);
		for (AvaliacaoUsado avaliacaoUsadoIterate : listaPorPlacaEStatus) {
			if (avaliacaoUsadoIterate.getDataAvaliacao() != null && avaliacaoUsadoIterate.getDiasValidade() != null) {
				LocalDateTime dataInicioVigencia = UtilsData.toLocalDateTime(avaliacaoUsadoIterate.getDataAvaliacao());
				LocalDateTime dataInformada = UtilsData.toLocalDateTime(dataInformadaCalendar);
				LocalDateTime dataFinalVigencia = dataInicioVigencia.plusDays(avaliacaoUsadoIterate.getDiasValidade());

				if (dataInicioVigencia.isBefore(dataInformada) && dataFinalVigencia.isAfter(dataInformada)) {
					retorno.add(avaliacaoUsadoIterate);
				}

			}
		}
		return retorno;
	}

	private List<AvaliacaoUsado> obterPorStatusEPlaca(String placa, String statusDaAvaliacao) {
		AvaliacaoStatusEnum enumRetorno = UtilsEnum.obterEnumLabeledPorLabel(AvaliacaoStatusEnum.values(),
				statusDaAvaliacao);
		List<AvaliacaoUsado> retorno = avaliacaoUsadoService.obterPorStatusEPlaca(placa, enumRetorno);
		return retorno;
	}

	@GET
	@Path("/{id}")
	public Response obterPorId(@PathParam("id") Integer id) {
		return Response.ok(avaliacaoUsadoService.obterPorId(Long.parseLong(id.toString()))).build();
	}

	@PUT
	public Response alterar(AvaliacaoUsado avaliacaoUsado) {
		avaliacaoUsadoService.alterar(avaliacaoUsado);
		return Response.ok().build();
	}

	@POST
	public Response incluir(AvaliacaoUsado avaliacaoUsado) {
		avaliacaoUsadoService.incluir(avaliacaoUsado);
		return Response.ok().build();
	}

	@DELETE
	@Path("/{id}")
	public Response remover(@PathParam("id") Long id) {
		avaliacaoUsadoService.deletar(id);
		return Response.ok().build();
	}

}
