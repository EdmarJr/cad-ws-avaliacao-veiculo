package br.com.gruposaga.cad.svs.ws;

import br.com.gruposaga.cad.wss.WsAbstrato;
import br.com.gruposaga.sdk.cad.jwt.service.ServiceFilter;
import org.slf4j.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

public abstract class WsAbstratoSvsFin extends WsAbstrato
{
    private @Inject
    WsClientTokenProvider tokenProvider;
    protected @Inject ServiceFilter serviceFilter;
    protected @Inject Logger logger;

    @PostConstruct
    private void init ()
    {
        tokenProvider.setToken(token());
    }

    protected String getCpfUsuario(){
        return serviceFilter.extrairDocColaboradorDoToken(token());
    }
}
