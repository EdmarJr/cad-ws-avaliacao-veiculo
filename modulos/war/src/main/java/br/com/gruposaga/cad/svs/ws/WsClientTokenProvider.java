package br.com.gruposaga.cad.svs.ws;

import br.com.gruposaga.cad.wsc.TokenProvider;
import br.com.gruposaga.sdk.cad.jwt.service.ServiceFilter;

import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import java.io.Serializable;

@ConversationScoped
public class WsClientTokenProvider implements TokenProvider, Serializable
{
    private static final long serialVersionUID = 1L;

    @Inject
    private ServiceFilter serviceFilter;
    private String        token;

    public void setToken (String token)
    {
        this.token = token;
    }

    @Override
    public String provide ()
    {
        return serviceFilter.extrairDocColaboradorDoToken(token);
    }
}
