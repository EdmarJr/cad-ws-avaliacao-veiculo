package br.com.gruposaga.cad.svs.fin.ws.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.svs.fin.model.entities.AvaliacaoUsado;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@Path(WebServicesSvsFin.AVALIACAO_USADOS)
@Api(value = "Crud Avaliacao usados do dealer")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface WsAvaliacaoUsados {

	@GET
	@ApiOperation("buscarUnicoResultadoOuListaDeResultadosPaginada")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response buscarUnicoResultadoOuListaDeResultadosPaginada(
			@QueryParam("parametrosDeBusca") String[] parametrosDeBusca,
			@QueryParam("parametrosDeOrdenacao") String[] parametrosDeOrdenacao, @QueryParam("offSet") Integer offSet,
			@QueryParam("limit") Integer limit, @QueryParam("placa") String placa,
			@QueryParam("statusDaAvaliacao") String statusDaAvaliacao, @QueryParam("dataVigente") Long dataVigente);

	@GET
	@Path("/{id}")
	@ApiOperation("obterPorId")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response obterPorId(@PathParam("id") Integer id);

	@PUT
	@ApiOperation("alterar")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response alterar(AvaliacaoUsado avaliacaoUsado);

	@POST
	@ApiOperation("incluir")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response incluir(AvaliacaoUsado avaliacaoUsado);

	@DELETE
	@Path("/{id}")
	@ApiOperation("remover")
	@ApiImplicitParams(@ApiImplicitParam(name = HttpHeaders.AUTHORIZATION, required = true, dataType = "String", value = "Token", paramType = "header"))
	public Response remover(@PathParam("id") Long id);

}
