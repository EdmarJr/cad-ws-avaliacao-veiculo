package com.svs.fin.model.entities;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.svs.fin.model.enums.AvaliacaoStatusEnum;

@Entity
@Table(name = "AvaliacaoUsado")
@JsonIgnoreProperties(ignoreUnknown = true)
@NamedQueries({
		@NamedQuery(name = AvaliacaoUsado.NQ_OBTER_POR_PLACA_E_STATUS.NAME, query = AvaliacaoUsado.NQ_OBTER_POR_PLACA_E_STATUS.JPQL) })
public class AvaliacaoUsado {

	public static interface NQ_OBTER_POR_PLACA_E_STATUS {
		public static final String NAME = "AvaliacaoUsado.obterPorPlacaEStatus";
		public static final String JPQL = "Select au from AvaliacaoUsado au where au.status = :"
				+ NQ_OBTER_POR_PLACA_E_STATUS.NM_PM_STATUS + " AND au.placa =:"
				+ NQ_OBTER_POR_PLACA_E_STATUS.NM_PM_PLACA;
		public static final String NM_PM_STATUS = "status";
		public static final String NM_PM_PLACA = "placa";
	}
	
	@Column(name = "AvaliacaoUsado_Codigo")
	@Id
	private Long codigo;
	@Column(name = "AvaliacaoUsado_Placa")

	private String placa;

	@ManyToOne
	@JoinColumn(name = "AvaliacaoUsado_ModeloVeiculoCod", referencedColumnName = "ModeloVeiculo_Codigo")
	private ModeloVeiculo modeloVeiculo;

	@Column(name = "AvaliacaoUsado_PrecoAvaliacao")
	private Double preco;
	@Column(name = "AvaliacaoUsado_Status")
	@Enumerated(EnumType.STRING)
	private AvaliacaoStatusEnum status;

	@Column(name = "AvaliacaoUsado_DiasValidade")
	private Integer diasValidade;
	@Column(name = "AvaliacaoUsado_DataAvaliacao")
	private Calendar dataAvaliacao;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public ModeloVeiculo getModeloVeiculo() {
		return modeloVeiculo;
	}

	public void setModeloVeiculo(ModeloVeiculo modeloVeiculo) {
		this.modeloVeiculo = modeloVeiculo;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public AvaliacaoStatusEnum getStatus() {
		return status;
	}

	public void setStatus(AvaliacaoStatusEnum status) {
		this.status = status;
	}

	public Integer getDiasValidade() {
		return diasValidade;
	}

	public void setDiasValidade(Integer diasValidade) {
		this.diasValidade = diasValidade;
	}

	public Calendar getDataAvaliacao() {
		return dataAvaliacao;
	}

	public void setDataAvaliacao(Calendar dataAvaliacao) {
		this.dataAvaliacao = dataAvaliacao;
	}

	// @Column(name="AvaliacaoUsado_VeiculoAnoCod")
	// @Column(name="AvaliacaoUsado_Chassi")
	// @Column(name="AvaliacaoUsado_NroRenavam")
	// @Column(name="AvaliacaoUsado_Preferencia")
	// @Column(name="Atendimento_Codigo")
	// @Column(name="AvaliacaoUsado_UsuarioCod")
	// @Column(name="AvaliacaoUsado_PessoaCodProprietario")
	// @Column(name="AvaliacaoUSado_NotaFiscalCod")
	// @Column(name="AvaliacaoUsado_FinalidadeCompraCod")
	// @Column(name="AvaliacaoUsado_ValorSugeridoVendaSistema")
	// @Column(name="AvaliacaoUsado_EstadoCod_Placa")
	// @Column(name="AvaliacaoUsado_ProprietarioDoc")
	// @Column(name="AvaliacaoUsado_NotaFiscalItemCod")
	// @Column(name="AvaliacaoUsado_EmpresaCod")
	// @Column(name="AvaliacaoUsado_Kilometragem")
	// @Column(name="AvaliacaoUsado_CorCod_Externa")
	// @Column(name="AvaliacaoUsado_MotAvaliacaoCod")
	// @Column(name="AvaliacaoUsado_Observacao")
	// @Column(name="AvaliacaoUsado_PrevisaoVenda")
	// @Column(name="AvaliacaoUsado_PrevisaoRecebe")
	// @Column(name="AvaliacaoUsado_PrevisaoReparo")
	// @Column(name="AvaliacaoUsado_PrecoCliente")
	// @Column(name="AvaliacaoUsado_ValorTOP")
	// @Column(name="AvaliacaoUsado_ValorSugeridoVenda")

	// SELECT T1.[AvaliacaoUsado_Placa],
	// T1.[AvaliacaoUsado_Status],
	// T1.[AvaliacaoUsado_PrecoAvaliacao],
	// T1.[AvaliacaoUsado_ModeloVeiculoCod] AS AvaliacaoUsado_ModeloVeiculoCod,
	// T2.[ModeloVeiculo_Descricao] AS AvaliacaoUsado_ModeloVeiculoDes,
	// T1.[AvaliacaoUsado_Codigo],
	// T1.[AvaliacaoUsado_DiasValidade],
	// T1.[AvaliacaoUsado_DataAvaliacao]
	// FROM ([AvaliacaoUsado] T1 WITH (NOLOCK)
	// INNER JOIN [ModeloVeiculo] T2 WITH (NOLOCK) ON T2.[ModeloVeiculo_Codigo] =
	// T1.[AvaliacaoUsado_ModeloVeiculoCod])
	// WHERE (T1.[AvaliacaoUsado_Placa] = 'KBG5973
	// ')--@AV55PropostaParcela_PlacaVeiculoUsado)
	// AND (T1.[AvaliacaoUsado_Status] = 'AUT')
	// ORDER BY T1.[AvaliacaoUsado_Placa],
	// T1.[AvaliacaoUsado_Codigo] --',N'@AV55PropostaParcela_PlacaVeiculoUsado
	// char(8)',@AV55PropostaParcela_PlacaVeiculoUsado='KBG5973 '

}
