package com.svs.fin.model.enums;

import br.com.magmatecnologia.architecture.utils.common.enums.EnumLabeled;

public enum AvaliacaoStatusEnum implements EnumLabeled{
	AUT("Autorizado"),
	CAN("Cancelado"),
	PEN("Pendente");

	
	private String label;
	private AvaliacaoStatusEnum(String label) {
		this.label = label;
		// TODO Auto-generated constructor stub
	}
	@Override
	public String getLabel() {
		return label;
	}
}
