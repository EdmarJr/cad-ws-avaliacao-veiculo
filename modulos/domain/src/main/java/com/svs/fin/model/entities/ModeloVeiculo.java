package com.svs.fin.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Table(name="ModeloVeiculo")
@Entity
@JsonIgnoreProperties(ignoreUnknown=true)
public class ModeloVeiculo {
	@Id
	@Column(name="ModeloVeiculo_Codigo")
	private Long codigo;
	
//	@Column(name="ModeloVeiculo_Descricao")
//	@Column(name="ModeloVeiculo_MarcaCod")
//	@Column(name="ModeloVeiculo_ModeloMarca")
//	@Column(name="ModeloVeiculo_GrupoModeloCod")
//	@Column(name="ModeloVeiculo_CombustivelCod")
//	@Column(name="ModeloVeiculo_FamiliaVeiculoCod_Nov")
//	@Column(name="ModeloVeiculo_FamiliaVeiculoCod_Usa")
//	@Column(name="ModeloVeiculo_NroPortas")
//	@Column(name="ModeloVeiculo_QtdePassageiros")
//	@Column(name="ModeloVeiculo_MesesGarantia")
//	@Column(name="ModeloVeiculo_KmGarantia")
//	@Column(name="ModeloVeiculo_NCMCod")
//	@Column(name="ModeloVeiculo_DiasPgtoAposVenda")
//	@Column(name="ModeloVeiculo_TempoMaxBloqueio")
//	@Column(name="ModeloVeiculo_TempoMaxReserva")
//	@Column(name="ModeloVeiculo_MotorizacaoCod")
//	@Column(name="ModeloVeiculo_TransmissaoCod")
//	@Column(name="ModeloVeiculo_Potencia")
//	@Column(name="ModeloVeiculo_Cilindrada")
//	@Column(name="ModeloVeiculo_TMOCod_Revisao")
//	@Column(name="ModeloVeiculo_ImagemVistoriaCod")
//	@Column(name="ModeloVeiculo_Ativo")
//	@Column(name="ModeloVeiculo_UsuCodCriacao")
//	@Column(name="ModeloVeiculo_DataCriacao")
//	@Column(name="ModeloVeiculo_UsuCodAlteracao")
//	@Column(name="ModeloVeiculo_DataAlteracao")
//	@Column(name="ModeloVeiculo_TempoMaxTipo")
//	@Column(name="ModeloVeiculo_DistanciaEixos")
//	@Column(name="ModeloVeiculo_CapacidadeTracao")
//	@Column(name="ModeloVeiculo_TipoBancoPedido")
//	@Column(name="ModeloVeiculo_PesoLiquido")
//	@Column(name="ModeloVeiculo_PesoBruto")
//	@Column(name="ModeloVeiculo_Especie")
//	@Column(name="ModeloVeiculo_Tipo")
//	@Column(name="ModeloVeiculo_ProcedenciaCod")
//	@Column(name="ModeloVeiculo_LinkDemonstracao")
//	@Column(name="ModeloVeiculo_TabelaMolicar")
//	@Column(name="ModeloVeiculo_CapacidadeCarga")
//	@Column(name="ModeloVeiculo_Comprimento")
//	@Column(name="ModeloVeiculo_Largura")
//	@Column(name="ModeloVeiculo_Altura")
//	@Column(name="ModeloVeiculo_IdNCMCargaTributaria")
//	@Column(name="ModeloVeiculo_AnoFim")
//	@Column(name="ModeloVeiculo_AnoInicio")
//	@Column(name="ModeloVeiculo_GrupoModeloCod_Usados")
//	@Column(name="ModeloVeiculo_CodigoFINAME")
}
