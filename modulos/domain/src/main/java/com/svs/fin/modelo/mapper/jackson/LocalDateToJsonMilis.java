package com.svs.fin.modelo.mapper.jackson;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class LocalDateToJsonMilis extends StdSerializer<LocalDate> {

    private static final long serialVersionUID = 1L;

    public LocalDateToJsonMilis() {
        super(LocalDate.class);
    }

	@Override
	public void serialize(LocalDate value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		if(value != null) {
			gen.writeNumber(value.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli());
		}
		
	}

}