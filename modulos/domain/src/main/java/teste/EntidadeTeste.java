package teste;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class EntidadeTeste implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SEQ_ENTIDADE_TESTE")
    @SequenceGenerator(name="SEQ_ENTIDADE_TESTE", sequenceName="SEQ_ENTIDADE_TESTE", allocationSize = 1)
    private Long id;

    @Column(length = 300)
    private String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
